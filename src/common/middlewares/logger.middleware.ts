import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private logger = new Logger('HTTP');

  use(req: Request, res: Response, next: NextFunction) {
    // finish event, response가 완료 됐을 떄
    res.on('finish', () => {
      this.logger.log(
        `${req.ip} ${req.method} ${req.statusCode}`,
        req.originalUrl,
      );
    });
    this.logger.log(req.ip, req.originalUrl);
    next();
  }
}
